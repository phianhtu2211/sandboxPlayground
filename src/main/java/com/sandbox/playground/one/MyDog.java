package com.sandbox.playground.one;

import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class MyDog {

    /**
        @EventListener sẽ lắng nghe mọi sự kiện xảy ra
        Nếu có một sự kiện DoorBellEvent được bắn ra, nó sẽ đón lấy
        và đưa vào hàm để xử lý

        Noted: Nếu không để @Async thì lúc này code sẽ xử lý đồng bộ synchronous
                Ngược lại nếu là @Async lúc này code sẽ xử lý bất đồng bộ asynchronous. Tức là người bấm chuông cứ bấm, chó nghe thấy tiếng cứ sủa,
                mỗi người một viêc không ảnh hưởng tới ai cả, chỉ biết chắc chắn sự kiện sẽ phản ứng là được.
                Hay có thể hiểu là sources gây ra event cứ tạo event, subcriber là con chó nghe thấy publisher event thì cứ sủa.
                Mô hình @Async này có thể liên tưởng tới Observer Pattern.
     */
    @Async
    @EventListener
    public void doorBellEventListener(DoorBellEvent doorBellEvent) throws InterruptedException {
        // Giả sử con chó đang ngủ, 1 giây sau mới dậy
        Thread.sleep(1000);
        // Sự kiện DoorBellEvent được lắng nghe và xử lý tại đây
        System.out.println(Thread.currentThread().getName() + ": Chó ngủ dậy!!!");
        System.out.println(String.format("%s: Go go!! Có người tên là %s gõ cửa!!!", Thread.currentThread().getName(), doorBellEvent.getGuestName()));
    }
}
