package com.sandbox.playground;

import com.sandbox.playground.one.MyHouse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class PlaygroundApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlaygroundApplication.class, args);
    }

    /*
        https://viblo.asia/p/spring-boot-xu-ly-su-kien-voi-ateventlistener-atasync-djeZ1bXglWz
        https://github.com/loda-kun/spring-boot-learning
        https://gpcoder.com/5112-gioi-thieu-aspect-oriented-programming-aop/
        https://docs.spring.io/spring-framework/docs/3.0.0.M3/reference/html/ch08s06.html?fbclid=IwAR3z9uBLBMHtq-20QZiRul5zEiEvmc0c3fVpBRjafQQhb8AdprzI-GAPzvA
     */
    @Autowired
    MyHouse myHouse;

    @Bean
    CommandLineRunner run() {
        return args -> {
            System.out.println(Thread.currentThread().getName() + ": Tu đi tới cửa nhà !!!");
            System.out.println(Thread.currentThread().getName() + ": => Tu bấm chuông và khai báo họ tên!");
            // gõ cửa
            myHouse.rangDoorbellBy("Tu");
            System.out.println(Thread.currentThread().getName() +": Tu quay lưng bỏ đi");
        };
    }

}
